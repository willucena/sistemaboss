<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function (){
    return 'Tenant';
});


Route::resource('companies', \App\Http\Controllers\Tenant\CompanyController::class);
